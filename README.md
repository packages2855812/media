Installation

```shell
composer require wpm/furs
```

Publish config migrations

```shell
php artisan vendor:publish
 ```

Add FursServiceProviders to app.php

```php
   ...
   /*
   * Package Service Providers...
   */
   \wpm\furs\FursServiceProvider::class,
   ...
```

Premise reporting:

```php
$premise = Premise::create($data);
$cert = new fuCert($premise->getStorageCertPath(), $premise->cert_password);
$premise->update(["TaxNumber" => intval($cert->taxNum)]);
$premise->report();
```

Invoice reporting:

```php
$invoice = Invoice::create($data);
$InvoiceNumber = $premise->BusinessPremiseID . "-" . $cash_register->number . "-" . $invoice->id;
$invoice->update(compact("InvoiceNumber"));
$invoice->report();
```

Copy furs.php file from config to your config folder.

Test certificate password

```
30VPSPYYEUFC
```
