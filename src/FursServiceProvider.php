<?php

namespace D3x\Media;

use d3x\starter\StarterServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use wpm\furs\Traits\PublishesMigrations;
use function wpm\furs\config_path;
use function wpm\furs\resource_path;


class FursServiceProvider extends ServiceProvider
{
    use PublishesMigrations;


    public function boot()
    {
        $this->registerMigrations(__DIR__ . '/database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'furs');
        $this->publishes([
            __DIR__ . '/resources/js/' =>
                resource_path('/vendor/furs'
                )], 'furs-app');
//        Route::middleware('web')->group(function () {
//            $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
//        });
        $this->publishes([
            __DIR__ . '/config/furs.php' => config_path('furs.php'),
        ]);
    }

    public function register()
    {
        $this->app->register(StarterServiceProvider::class);
        $this->mergeConfigFrom(
            __DIR__ . '/config/furs.php', 'furs'
        );
    }
}
